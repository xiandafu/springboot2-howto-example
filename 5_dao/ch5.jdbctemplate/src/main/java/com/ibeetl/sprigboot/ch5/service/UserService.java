package com.ibeetl.sprigboot.ch5.service;

import com.ibeetl.sprigboot.ch5.entity.User;

public interface UserService {
	public User geUserById(Long id) ;
	
}
