package com.ibeetl.springboot.ch1.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloWorldController {
  @RequestMapping("/say.html")
  @ResponseBody
  public  String say(){
    return "hello world springboot!";
  }
}
