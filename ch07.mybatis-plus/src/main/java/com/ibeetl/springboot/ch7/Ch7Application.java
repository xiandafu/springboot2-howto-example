package com.ibeetl.springboot.ch7;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.ibeetl.springboot.ch7.dao")
public class Ch7Application {
  public static  void main(String[] args){
    SpringApplication.run(Ch7Application.class, args);
  }

}
