package com.ibeetl.springboot.ch7.controller;


import com.ibeetl.springboot.ch7.service.UserService;
import com.ibeetl.springboot.ch7.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class HelloworldController {
	public HelloworldController(){
		int a =1;
	}

	@Autowired
	UserService userService;

	@RequestMapping("/index.html")
	@ResponseBody
	public String hello(){
		return "hello index.html";
	}

	@RequestMapping("/user/id/{id}")
	@ResponseBody
	public User query(@PathVariable Integer id){
		return userService.getUserById(id);
	}

	@RequestMapping("/user/name/{name}")
	@ResponseBody
	public List<User> query(@PathVariable String name){
		return userService.select(name);
	}

	@RequestMapping("/user/add/{name}")
	@ResponseBody
	public int  add(@PathVariable String name){
		return userService.addUser(name);
	}


	@RequestMapping("/user/update/{id}/{name}")
	@ResponseBody
	public void   update(@PathVariable String name,@PathVariable Integer id){
		userService.updateUser(id,name);
	}


	@RequestMapping("/user/remove/{id}")
	@ResponseBody
	public void   remove(@PathVariable Integer id){
		userService.removeUser(id);
	}


}