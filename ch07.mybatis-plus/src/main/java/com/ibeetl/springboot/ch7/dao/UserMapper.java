package com.ibeetl.springboot.ch7.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ibeetl.springboot.ch7.entity.User;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserMapper  extends BaseMapper<User> {
//    @Select("SELECT * FROM  USER WHERE id = #{id}")
//    public User selectById(Integer id);

    public List<User> selectByName(String name);

    public void addUser(User user);

    public int updateUser(Integer id ,String name);

    public int removeUser(Integer id);
}
