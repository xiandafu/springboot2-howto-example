package com.ibeetl.springboot.ch7.service;




import com.ibeetl.springboot.ch7.entity.User;

import java.util.List;

public interface UserService {
	public User getUserById(Integer id);
	public List<User> select(String name);
	public int  addUser(String name);
	public void updateUser(Integer id,String name);
	public void removeUser(Integer id);



}
