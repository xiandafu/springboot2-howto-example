package com.ibeetl.springboot.ch7.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ibeetl.springboot.ch7.dao.UserMapper;
//import com.ibeetl.springboot.ch7.mapper.UserMapper;
import com.ibeetl.springboot.ch7.service.UserService;
import com.ibeetl.springboot.ch7.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	@Autowired
	UserMapper userDao;

	public User getUserById(Integer id) {
		return userDao.selectById(id);
	}

	@Override
	public List<User> select(String name) {

		List<User> users = userDao.selectByName(name);
		return users;


	}

	public int  addUser(String name){
		User user = new User();
		user.setName(name);
		user.setDepartmentId(1);
		user.setCreateTime(new Date());
		userDao.addUser(user);
		return user.getId();
	}

	public void updateUser(Integer id,String name){
		userDao.updateUser(id,name);
	}

	public void removeUser(Integer id){
		userDao.removeUser(id);
	}
}
