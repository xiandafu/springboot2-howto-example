package com.ibeetl.springboot.ch10.dao;

import org.beetl.sql.core.mapper.BaseMapper;

import com.ibeetl.springboot.ch10.entity.User;

public interface UserDao extends BaseMapper<User> {

}
