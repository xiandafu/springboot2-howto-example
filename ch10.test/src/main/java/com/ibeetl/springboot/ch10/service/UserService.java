package com.ibeetl.springboot.ch10.service;

import com.ibeetl.springboot.ch10.entity.User;

public interface UserService {
	public int getCredit(int userId);
	public boolean updateUser(User user);
}
