package com.ibeetl.springboot.ch10.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibeetl.springboot.ch10.dao.UserDao;
import com.ibeetl.springboot.ch10.entity.User;
import com.ibeetl.springboot.ch10.service.CreditSystemService;
import com.ibeetl.springboot.ch10.service.UserService;
@Service
@Transactional
public class UserServiceImpl implements UserService {

	Log log = LogFactory.getLog(this.getClass());
	@Autowired
	CreditSystemService creditSystemService;
	
	@Autowired
	UserDao userDao;
	
	@Override
	public int getCredit(int userId) {
	
		return creditSystemService.getUserCredit(userId);
		
	}

	@Override
	public boolean updateUser(User user) {
		int ret = userDao.updateById(user);
		return ret==1;
	}

}
