package com.ibeetl.springboot.ch11.service;

import com.ibeetl.springboot.ch11.entity.AuditInfo;

public interface AuditService {

  public void log(AuditInfo info);
}

