package com.ibeetl.springboot.ch11.service.impl;

import com.ibeetl.springboot.ch11.entity.AuditInfo;
import com.ibeetl.springboot.ch11.exception.AppException;
import com.ibeetl.springboot.ch11.service.AuditService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class AuditServiceImpl implements AuditService {
  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW,rollbackFor = AppException.class)
  public void log(AuditInfo info) {

  }
}
