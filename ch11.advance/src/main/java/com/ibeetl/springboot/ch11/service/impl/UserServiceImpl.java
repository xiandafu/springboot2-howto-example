package com.ibeetl.springboot.ch11.service.impl;

import com.ibeetl.springboot.ch11.service.UserService;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class UserServiceImpl implements UserService {

  @Transactional(readOnly = true)
  public int queryCount() {
    return 0;
  }
}
