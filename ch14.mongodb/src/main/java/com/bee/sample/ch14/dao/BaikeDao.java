package com.bee.sample.ch14.dao;


import com.bee.sample.ch14.entity.Baike;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.*;

import java.util.List;

public interface BaikeDao extends MongoRepository<Baike, String> {

  Page<Baike> findAll(Pageable pageable);
  List<Baike> queryByStatus(int status);


}
