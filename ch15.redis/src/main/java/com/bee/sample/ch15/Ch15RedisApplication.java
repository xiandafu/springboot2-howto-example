package com.bee.sample.ch15;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch15RedisApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ch15RedisApplication.class, args);
	}
}
