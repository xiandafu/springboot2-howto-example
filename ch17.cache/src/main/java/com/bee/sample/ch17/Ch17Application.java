package com.bee.sample.ch17;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class Ch17Application {

	public static void main(String[] args) {
		SpringApplication.run(Ch17Application.class, args);
	}
}
