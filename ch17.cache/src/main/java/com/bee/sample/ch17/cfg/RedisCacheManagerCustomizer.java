package com.bee.sample.ch17.cfg;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext.SerializationPair;
/**
 * 配置缓存,这个项目只能配置RedisCacheManagerCustomizer或者使用一二级别缓存CacheConfig
 * @author xiandafu
 *
 */
//@Configuration
public class RedisCacheManagerCustomizer {
	@Bean
	public RedisCacheManager getRedisCacheManager(RedisConnectionFactory connectionFactory){
		//自定义redis的序列化方式
		RedisCacheWriter cacheWriter = RedisCacheWriter.lockingRedisCacheWriter(connectionFactory);
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		JdkSerializationRedisSerializer jdkSerializer = new JdkSerializationRedisSerializer(loader);
		SerializationPair<Object> pair = SerializationPair.fromSerializer(jdkSerializer);
		//设置所有的超时时间
		RedisCacheConfiguration cacheConfig = RedisCacheConfiguration.defaultCacheConfig().serializeValuesWith(pair);
		cacheConfig = cacheConfig.entryTtl(Duration.ofSeconds(3600));

		//设置单个缓存的超时时间
		Map<String, RedisCacheConfiguration> initialCacheConfigurations = new HashMap<>();
		initialCacheConfigurations.put("user",cacheConfig.entryTtl(Duration.ofSeconds(60)));
		
		
		RedisCacheManager cacheManager = new RedisCacheManager(cacheWriter, cacheConfig,initialCacheConfigurations);
		
		return cacheManager;
	}	
}
