package com.bee.sample.ch17.service;

import com.bee.sample.ch17.entity.Menu;
import com.bee.sample.ch17.pojo.MenuNode;

public interface MenuService {
	public void addMenu(Menu menu);
	public Menu getMenu(Long id);
	public MenuNode getMenuTree();
}
