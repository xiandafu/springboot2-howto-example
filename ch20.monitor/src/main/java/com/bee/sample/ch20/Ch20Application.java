package com.bee.sample.ch20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch20Application {

	public static void main(String[] args) {
		SpringApplication.run(Ch20Application.class, args);
	}
}
