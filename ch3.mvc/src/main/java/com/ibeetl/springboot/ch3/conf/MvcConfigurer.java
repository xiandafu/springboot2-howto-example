package com.ibeetl.springboot.ch3.conf;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.*;

import com.ibeetl.springboot.ch3.entity.User;
import org.springframework.web.servlet.resource.PathResourceResolver;

import java.util.concurrent.TimeUnit;

@Configuration
public class MvcConfigurer implements WebMvcConfigurer {
	
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new SessionHandlerInterceptor()).addPathPatterns("/user/**");
	}
	public void addCorsMappings(CorsRegistry registry) {
	}
	
	public void addFormatters(FormatterRegistry registry) {
		 registry.addFormatter(new DateFormatter("yyyy-MM-dd HH:mm:ss"));
		
	}


	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/index.html").setViewName("/index.ftl");
		registry.addRedirectViewController("/**/*.do", "/index.html");
	}

//  public void addResourceHandlers(ResourceHandlerRegistry registry) {
////    registry.addResourceHandler("/*.js", "/*.css","/*.png")
////      .addResourceLocations("classpath:/static/")
////      .setCacheControl(CacheControl.maxAge(12, TimeUnit.HOURS)
////        .cachePrivate()
////        .mustRevalidate())
////      .resourceChain(true)
////      .addResolver(new PathResourceResolver());
//
//
//  }
	
	/**
	 * 
	 * 检查用户是否已经登录，如果未登录，重定向到登录页面
	 *
	 */
	class SessionHandlerInterceptor implements HandlerInterceptor{
		public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
				throws Exception {
			User user = (User) request.getSession().getAttribute("user"); 
			if(user==null){
				response.sendRedirect("/login.html");
				return false;
			}
			return true;
		}
	}
	
	

}
