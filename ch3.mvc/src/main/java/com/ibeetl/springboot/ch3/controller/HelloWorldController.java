package com.ibeetl.springboot.ch3.controller;

import com.ibeetl.springboot.ch3.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/test")
public class HelloWorldController {
  @RequestMapping("/index.html")
  public  String say(Model model){
    model.addAttribute("name","hello,world");
    return "/index";
  }

  @RequestMapping("/index.json")
  @ResponseBody
  public User say(){
    return  new User();
  }
}
