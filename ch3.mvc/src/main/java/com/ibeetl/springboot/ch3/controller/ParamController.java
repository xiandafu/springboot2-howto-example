package com.ibeetl.springboot.ch3.controller;

import com.ibeetl.springboot.ch3.controller.form.OrderPostForm;
import com.ibeetl.springboot.ch3.controller.form.WorkInfoForm;
import com.ibeetl.springboot.ch3.entity.User;
import com.ibeetl.springboot.ch3.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

/**
 * mvc param映射测试
 * @author lijiazhi
 *
 */
@Controller
@RequestMapping("/param")
public class ParamController {
	
	@Autowired UserService userService;
	@GetMapping(path = "/user/{id}.json")
	@ResponseBody
	public User getUser(@PathVariable Long id) {
		return userService.getUserById(id);
	}

	@GetMapping(path = "/user/{id}.html")
	public String getUser(@PathVariable Long id, Model model) {
		User userInfo =  userService.getUserById(id);
		//model.addAttribute(userInfo); 与下面代码效果相同
		model.addAttribute("user", userInfo);
		return "/userInfo";
	}

	@GetMapping(path = "/user/2/{id}.html")
	public ModelAndView getUser2(@PathVariable Long id, ModelAndView view) {
		User userInfo =  userService.getUserById(id);
		view.addObject("user", userInfo);
		view.setViewName("/userInfo");
		return view;
	}

	@GetMapping(path = "/update2.json")
	@ResponseBody
	public String updateUserName(Integer id, String name) {

		return "success";
	}

	@GetMapping(path = "/update.json")
	@ResponseBody
	public String updateUser(User user) {
		System.out.println(user.getName());
		System.out.println(user.getId());
		return "success";
	}

	@PostMapping(path = "/saveOrder.json")
	@ResponseBody
	public String saveOrder( OrderPostForm form) {
		return "success";
	}

	@PostMapping(path = "/updateUser.json")
	@ResponseBody
	public String saveOrderByJson(@RequestBody User user) {
		return "succcess";
	}

	@PostMapping("/form")
	@ResponseBody
	public String handleFormUpload(
			MultipartFile file) throws IOException {
		if (file.isEmpty()) {
			/*作者注:先结束次要路径是个好的编码习惯，让代码阅读更轻松*/
			return "failure";
		}
		String fileName = file.getOriginalFilename();
		InputStream ins = file.getInputStream();
		// 处理上传内容
		return "success";

	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addCustomFormatter(new DateFormatter("yyyy-MM-dd"));
		binder.addValidators(new Validator(){

			@Override
			public boolean supports(Class<?> clazz) {
				return clazz== WorkInfoForm.class;
			}

			@Override
			public void validate(Object target, Errors errors) {
				WorkInfoForm form = (WorkInfoForm)target;
				;
			}

		});
	}

	@ResponseBody
	@RequestMapping("/date")
	public void printDate(Date d) {
		System.out.println(d);
		return ;
	}


}
