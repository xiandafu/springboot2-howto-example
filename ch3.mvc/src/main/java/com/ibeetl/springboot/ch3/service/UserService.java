package com.ibeetl.springboot.ch3.service;

import java.util.List;

import com.ibeetl.springboot.ch3.entity.User;

public interface UserService {
	public List<User> allUser();
	public User getUserById(Long id);
	public void updateUser(Long id,Integer type);
}
