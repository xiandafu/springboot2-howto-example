package com.ibeetl.springboot.ch4;

import org.beetl.ext.spring.BeetlEnable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@BeetlEnable
public class Ch4Application {

	public static void main(String[] args) {
		SpringApplication.run(Ch4Application.class, args);
	}

}
