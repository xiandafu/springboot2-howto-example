package com.ibeetl.springboot.ch4.conf;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.beetl.core.GroupTemplate;
import org.beetl.core.tag.Tag;
import org.beetl.core.tag.TagFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.ibeetl.springboot.ch4.util.SimpleFunction;
import com.ibeetl.springboot.ch4.util.SimpleTag;

@Configuration
public class BeetlExtConfig  {
	@Autowired
	GroupTemplate groupTemplate;
	@Autowired
	ApplicationContext applicationContext;
	
	@Autowired
	Environment env;
	@Autowired
	SimpleFunction simpleFunction;
	
	@PostConstruct
	public void config(){
		Map<String, Object> shared = new HashMap<String, Object>();
		shared.put("jsVersion", System.currentTimeMillis());
		groupTemplate.registerFunction("hi",simpleFunction);

		groupTemplate.registerTagFactory("myTag", new TagFactory(){
			public Tag createTag() {
				return  applicationContext.getBean(SimpleTag.class);
			}
			
		});


		
	}
		
	
	
}
