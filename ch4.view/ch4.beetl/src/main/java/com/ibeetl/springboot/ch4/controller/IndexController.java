package com.ibeetl.springboot.ch4.controller;

import com.ibeetl.springboot.ch4.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
/**
 * 学习beetl语法，可以参考在线体验:http://ibeetl.com/beetlonline/,学习大部分语法
 * @author xiandafu
 *
 */
@Controller
public class IndexController {
	@RequestMapping("/index.do")
	public ModelAndView  index(){
		ModelAndView view = new ModelAndView("/index.html");
		User user = new User();
		user.setId(11);
		user.setName("lijz");
		view.addObject("user", user);
		return view;
	}
}
