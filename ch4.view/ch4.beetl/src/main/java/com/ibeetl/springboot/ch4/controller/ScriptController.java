package com.ibeetl.springboot.ch4.controller;

import org.beetl.core.GroupTemplate;
import org.beetl.core.Script;
import org.beetl.core.exception.ErrorInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 学习beetl语法，可以参考在线体验:http://ibeetl.com/beetlonline/,学习大部分语法
 * @author xiandafu
 *
 */
@Controller
public class ScriptController {

	@Autowired
	GroupTemplate groupTemplate;

	@RequestMapping("/sciprt.do")
	@ResponseBody
	public String  index(){
		Script script = groupTemplate.getScript("/script/simple.txt");
		script.binding("name","李家智");
		script.execute();;
		if(!script.isSuccess()){
			ErrorInfo info = script.getErrorInfo();
			return info.toString();
		}

		String ret = (String)script.getReturnValue();
		return ret;
	}
}
