package com.ibeetl.springboot.ch4.entity;

public class User {
  private Integer id;
  private String name;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name+"  ad!bc";
  }

  public void setName(String name) {
    this.name = name;
  }
}
