package com.ibeetl.springboot.ch6.controller;

import com.ibeetl.springboot.ch6.entity.User;
import com.ibeetl.springboot.ch6.service.UserService;
import org.beetl.core.exception.BeetlException;
import org.beetl.core.exception.ErrorInfo;
import org.beetl.core.resource.StringTemplateResourceLoader;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class UserController {

	@Autowired
	UserService userService;


	@RequestMapping("/user/id/{id}")
	@ResponseBody
	public User query(@PathVariable Integer id){
		return userService.getUserById(id);
	}

	@RequestMapping("/user/name/{name}")
	@ResponseBody
	public List<User> query(@PathVariable String name){
		return userService.select(name);
	}


	@RequestMapping("/depart/{id}")
	@ResponseBody
	public List<User> queryByDepartId(@PathVariable Integer id){
		return userService.selectByDepartId(id);
	}

	@RequestMapping("/depart/{departId}/name/{name}")
	@ResponseBody
	public List<User> queryByDepartId(@PathVariable Integer departId,@PathVariable String name){
		return userService.selectByDepartIdAndName(departId,name);
	}


	@RequestMapping("/query.do")
	@ResponseBody
	public PageQuery<User> queryByDepartId(String name){
		PageQuery query =  userService.selectByCondtion(name);
		return query;
	}


	@RequestMapping("/update/user/{id}/{name}")
	@ResponseBody
	public void updateName(@PathVariable Integer id,@PathVariable String name){
		userService.updateName(id,name);
	}



}
