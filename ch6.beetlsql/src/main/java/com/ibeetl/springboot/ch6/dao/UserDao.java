package com.ibeetl.springboot.ch6.dao;

import java.util.List;

import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.ibeetl.springboot.ch6.entity.User;

@SqlResource("account/user")
public interface UserDao extends BaseMapper<User> {

	public List<User> selectByDepartId(Integer departId);

	public List<User> selectUser(Integer departId,String name);

	public PageQuery<User> queryUserByCondition(PageQuery query);
}