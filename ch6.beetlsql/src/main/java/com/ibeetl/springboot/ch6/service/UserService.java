package com.ibeetl.springboot.ch6.service;

import java.util.List;

import com.ibeetl.springboot.ch6.entity.User;
import org.beetl.sql.core.engine.PageQuery;

public interface UserService {
	public User getUserById(Integer id);
	public List<User> select(String  name);
	public List<User> selectByDepartId(Integer id);
	public List<User> selectByDepartIdAndName(Integer id,String name);
	public PageQuery  selectByCondtion(String name);
	public void updateName(Integer id,String name);


}
