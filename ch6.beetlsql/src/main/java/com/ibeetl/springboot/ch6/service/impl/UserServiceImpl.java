package com.ibeetl.springboot.ch6.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.ibeetl.springboot.ch6.dao.UserDao;
import com.ibeetl.springboot.ch6.entity.User;
import com.ibeetl.springboot.ch6.service.UserService;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {	
	@Autowired 
	UserDao userDao;

	@Autowired
	@Qualifier("aSqlManagerFactoryBean")
	SQLManager sqlManager;

	@Transactional(readOnly=true)
	public User getUserById(Integer id) {
		return userDao.unique(id);
	}
	@Override
	@Transactional(readOnly=true,timeout = 1)
	public List<User> select(String name) {
		User template = new User();
		template.setName(name);
		List<User>  users = userDao.template(template);
		return users;

	}

	@Override
	public List<User> selectByDepartId(Integer id){
		return userDao.selectByDepartId(id);
	}

	@Override
	public List<User> selectByDepartIdAndName(Integer departId,String name){
		Map paras = new HashMap<String,Object>();
		paras.put("departId",departId);
		paras.put("nane",name);
		List<User>  list = sqlManager.select("acount.user.selectUser",User.class,paras);
		return list;
	}

	public PageQuery  selectByCondtion(String name){
		PageQuery query = new PageQuery();
		Map paras = new HashMap<String,Object>();
		paras.put("name",name);
		query.setParas(paras);
		userDao.queryUserByCondition(query)	;
		return query;

	}

	@Override
	@Transactional(timeout = 10)
	public void updateName(Integer id, String name) {
		User user = new User();
		user.setId(id);
		user.setName(name);
		userDao.updateTemplateById(user);


		user = userDao.unique(id);
		System.out.println(user.getId());


	}

}
