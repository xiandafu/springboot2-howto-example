selectByDepartId
===

    select * from user where department_id=#departId# order by create_time desc
    
selectUser
===

    select * from user where name like #"%"+name# and department_id=#departId#
    
queryUserByCondition
===

    select
    @pageTag(){
    a.*
    @}
    from user a  where 1 = 1
    @if(isNotEmpty(name)){
    and name=#name#
    @}
    @pageIgnoreTag(){
    order by a.id desc
    @}